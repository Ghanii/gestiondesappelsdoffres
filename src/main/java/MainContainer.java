
import jade.wrapper.AgentContainer;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.util.leap.Properties;
import jade.util.ExtendedProperties;
import jade.wrapper.ControllerException;

public class MainContainer {
    public static void main(String[] args) {

        try{
            System.out.println("hello maven");
            Runtime runtime = Runtime.instance();
            Properties properties = new ExtendedProperties();
            properties.setProperty(Profile.GUI, "true");

            Profile profile = new ProfileImpl(properties);
           // AgentContainer mainContainer = runtime.createMainContainer(profile);
            AgentContainer mainContainer = runtime.createMainContainer(profile);
            mainContainer.start();
        } catch (ControllerException e) {
            System.out.println(e);
        }


    }
}
