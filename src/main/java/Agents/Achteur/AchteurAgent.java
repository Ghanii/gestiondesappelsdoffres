package Agents.Achteur;

import Containers.AchteurContainer;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.gui.GuiAgent;
import jade.gui.GuiEvent;
import jade.lang.acl.ACLMessage;

public class AchteurAgent extends GuiAgent implements AchteurAgentInterface {

    private AchteurContainer gui;
    private AID[] vendeurPossible ;


    @Override
    public void onGuiEvent(GuiEvent guiEvent) {


    }

    @Override
    protected void setup() {
        System.out.println("achteur agent was created and being set up "+ this.getAID().getName());
        gui = (AchteurContainer) getArguments()[0];
        gui.setAchteurAgent(this);

        ParallelBehaviour parallelBehaviour = new ParallelBehaviour();
        addBehaviour(parallelBehaviour);

        parallelBehaviour.addSubBehaviour(new TickerBehaviour(this , 6000) {
            @Override
            protected void onTick() {

                DFAgentDescription df = new DFAgentDescription();
                ServiceDescription serviceDescription = new ServiceDescription();
                serviceDescription.setType("sales");
                df.addServices(serviceDescription);

                DFAgentDescription[] agentDescriptions = new DFAgentDescription[0];
                try {
                    agentDescriptions = DFService.search(myAgent , df);
                } catch (FIPAException e) {
                    e.printStackTrace();
                }
                vendeurPossible = new AID[agentDescriptions.length];

                for(int i=0 ; i< agentDescriptions.length ; i++) {
                    vendeurPossible[i] = agentDescriptions[i].getName();
                }



            }
        });

        addBehaviour(new CyclicBehaviour() {
            @Override
            public void action() {
                ACLMessage msg = receive();
                if(msg != null) {
                    GuiEvent guiEvent = new GuiEvent(this , 1);
                    guiEvent.addParameter(msg.getContent());
                    gui.showAgentMessage(guiEvent);

                    ACLMessage aclMessage = new ACLMessage(ACLMessage.CFP);
                    aclMessage.setContent(msg.getContent());

                    for (AID vendeur : vendeurPossible){
                        aclMessage.addReceiver(vendeur);
                    }
                    send(aclMessage);
                }
            }
        });


    }

    @Override
    protected void takeDown() {
        System.out.println("agent achteru was desctructed");
    }
}
