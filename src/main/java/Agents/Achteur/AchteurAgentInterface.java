package Agents.Achteur;

import jade.gui.GuiAgent;
import jade.gui.GuiEvent;

public interface AchteurAgentInterface  {

    public void onGuiEvent(GuiEvent guiEvent);
}
