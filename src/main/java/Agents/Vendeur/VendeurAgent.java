package Agents.Vendeur;

import Containers.VendeurContainer;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.gui.GuiAgent;
import jade.gui.GuiEvent;
import jade.lang.acl.ACLMessage;

public class VendeurAgent extends GuiAgent implements VendeurAgentInterface {

    private VendeurContainer gui ;

    @Override
    protected void setup() {
        gui = (VendeurContainer) getArguments()[0];
        gui.setVendeurAgent(this);
        System.out.println("vendeur agent was created !");

        ParallelBehaviour parallelBehaviour = new ParallelBehaviour();
        addBehaviour(parallelBehaviour);
        parallelBehaviour.addSubBehaviour(new OneShotBehaviour() {
            @Override
            public void action() {
                DFAgentDescription dfAgentDescription = new DFAgentDescription();
                dfAgentDescription.setName(getAID());
                ServiceDescription serviceDescription = new ServiceDescription();
                serviceDescription.setType("sales");
                serviceDescription.setName("sales book");
                dfAgentDescription.addServices(serviceDescription);
                try{
                    DFService.register(myAgent , dfAgentDescription);
                } catch (FIPAException E){
                    System.out.println(E);
                }

            }
        });


        parallelBehaviour.addSubBehaviour(new CyclicBehaviour() {
            @Override
            public void action() {
                ACLMessage msg = receive();
                if(msg != null) {

                    switch (msg.getPerformative()){
                        case ACLMessage.CFP :
                            GuiEvent guiEvent = new GuiEvent(this , 1);
                            guiEvent.addParameter(msg.getContent());
                            gui.showMsg(guiEvent);
                            break;
                        default:
                            break;
                    }

                }
            }
        });



    }

    @Override
    protected void takeDown() {
        System.out.println("vendeur agent was destructed");
    }

    @Override
    protected void onGuiEvent(GuiEvent guiEvent) {

    }
}
