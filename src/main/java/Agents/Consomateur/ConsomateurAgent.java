package Agents.Consomateur;

import Containers.ConsomateurContainer;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.gui.GuiAgent;
import jade.gui.GuiEvent;
import jade.lang.acl.ACLMessage;
import jade.wrapper.ControllerException;

public class ConsomateurAgent extends GuiAgent implements ConsomateurAgentInterface {

    private ConsomateurContainer gui ;


    @Override
    protected void setup() {
        gui = (ConsomateurContainer) getArguments()[0];
        gui.setConsomateurAgent(this);
        System.out.println("consomateur agent created !!"+ this.getAID().getName());



        addBehaviour(new CyclicBehaviour() {
            @Override
            public void action() {
                ACLMessage msg = receive();
                if(msg != null) {
                    GuiEvent guiEvent = new GuiEvent(this , 1);
                    guiEvent.addParameter(msg.getContent());
                    gui.showMessage(guiEvent);
                }

            }
        });
    }

    @Override
    protected void takeDown() {
        System.out.println("consomateur agent taked down !!"+ this.getAID().getName());
    }

    @Override
    protected void beforeMove() {
        try{
            System.out.println("before move" + this.getContainerController().getContainerName());
        } catch (ControllerException e) {
            System.out.println(e);
        }
    }

    @Override
    protected void afterMove() {
        try{
            System.out.println("after  move" + this.getContainerController().getContainerName());
        } catch (ControllerException e) {
            System.out.println(e);
        }
    }

    @Override
    public void onGuiEvent(GuiEvent guiEvent) {
        if(guiEvent.getType()==1) {
            ACLMessage aclMessage = new ACLMessage(ACLMessage.REQUEST);
            String livre = guiEvent.getParameter(0).toString();
            aclMessage.setContent(livre);
            aclMessage.addReceiver(new AID("achteur" , AID.ISLOCALNAME));
            send(aclMessage);
        }
    }
}
