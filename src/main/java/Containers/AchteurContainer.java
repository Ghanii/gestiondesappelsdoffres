package Containers;

import Agents.Achteur.AchteurAgent;
import Agents.Achteur.AchteurAgentInterface;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.gui.GuiEvent;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.ControllerException;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class AchteurContainer extends Application {

    private ObservableList<String> livres ;

    public AchteurAgentInterface getAchteurAgent() {
        return achteurAgent;
    }

    public void setAchteurAgent(AchteurAgent achteurAgent) {
        this.achteurAgent = achteurAgent;
    }

    private AchteurAgentInterface achteurAgent ;

    public static void main(String[] args) {

       launch(AchteurContainer.class);

    }


    public void startAchteurContainer() {
        try {
            Runtime achteurContainer = Runtime.instance();
            Profile profile = new ProfileImpl(false);
            profile.setParameter(Profile.MAIN_HOST , "localhost" );

            AgentContainer agentContainer = achteurContainer.createAgentContainer(profile);
            AgentController agentController = agentContainer.createNewAgent("achteur" , AchteurAgent.class.getName() , new Object[]{this});
            agentController.start();
        } catch (ControllerException e) {
            System.out.println(e);
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        startAchteurContainer();

        primaryStage.setTitle("achteur");
        BorderPane borderPane = new BorderPane();

        VBox vBox = new VBox();
        GridPane gridPane = new GridPane();
        livres = FXCollections.observableArrayList();
        ListView<String> listView = new ListView<String>(livres);
        gridPane.add(listView , 0,0);
        vBox.getChildren().add(gridPane);

        borderPane.setCenter(vBox);

        Scene scene = new Scene(borderPane , 400 , 400);
        primaryStage.setScene(scene);
        primaryStage.show();


    }

    public void showAgentMessage(GuiEvent guiEvent){
        System.out.println("helooo guiiii !!");
        String msg = guiEvent.getParameter(0).toString();
        livres.add(msg);
    }
}
