package Containers;

import Agents.Vendeur.VendeurAgent;
import Agents.Vendeur.VendeurAgentInterface;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.gui.GuiEvent;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.ControllerException;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class VendeurContainer extends Application {

    private ObservableList<String> vendeurs ;
    AgentContainer vendeurContainer ;

    private VendeurContainer vendContainer ;

    public VendeurAgentInterface getVendeurAgent() {
        return vendeurAgent;
    }

    public void setVendeurAgent(VendeurAgent vendeurAgent) {
        this.vendeurAgent = vendeurAgent;
    }

    private VendeurAgentInterface vendeurAgent ;

    public static void main(String[] args) {
        launch(VendeurContainer.class);
    }

    public void startContainer(){
        try{
            Runtime runtime = Runtime.instance();
            Profile profile = new ProfileImpl(false);
            profile.setParameter(Profile.MAIN_HOST , "localhost");
            vendeurContainer = runtime.createAgentContainer(profile);
        } catch (Exception e){
            System.out.println(e);
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        startContainer();
        vendContainer = this ;

        primaryStage.setTitle("vendeur");
        BorderPane borderPane = new BorderPane();


        HBox hBox = new HBox();
        hBox.setSpacing(10);
        Label labelVendeur = new Label("vendeur name :");


        TextField textFieldVendeur = new TextField();
        Button buttonDeploy = new Button("deploy");

        hBox.getChildren().add(labelVendeur);
        hBox.getChildren().add(textFieldVendeur);
        hBox.getChildren().add(buttonDeploy);

        VBox vBox = new VBox();
        GridPane gridPane = new GridPane();
        vendeurs = FXCollections.observableArrayList();
        ListView<String> listView = new ListView<>(vendeurs);
        gridPane.add(listView , 0,0);
        vBox.getChildren().add(gridPane);

        borderPane.setCenter(vBox);


        borderPane.setTop(hBox);

        Scene scene = new Scene(borderPane , 400 , 400);
        primaryStage.setScene(scene);
        primaryStage.show();

        deployAgent(buttonDeploy ,textFieldVendeur );
    }

    private void deployAgent( Button buttonDeploy , TextField vendeurTextField ) {
        buttonDeploy.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String nameVendeur = vendeurTextField.getText();
                vendeurs.add(nameVendeur);
                try{
                    AgentController agentController = vendeurContainer.createNewAgent(nameVendeur , VendeurAgent.class.getName() ,new Object[]{vendContainer});
                    agentController.start();

                } catch (ControllerException e){
                    System.out.println(e);
                }
            }
        });
    }

    public void showMsg(GuiEvent guiEvent){
        String msg = guiEvent.getParameter(0).toString();
        vendeurs.add(msg);
    }
}
