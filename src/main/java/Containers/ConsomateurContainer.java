package Containers;

import Agents.Consomateur.ConsomateurAgent;
import Agents.Consomateur.ConsomateurAgentInterface;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.gui.GuiEvent;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.ControllerException;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;



public class ConsomateurContainer extends Application {

    private ConsomateurAgentInterface consomateurAgent ;

    private ObservableList<String> livres ;

    public ConsomateurAgentInterface getConsomateurAgent() {
        return consomateurAgent;
    }

    public void setConsomateurAgent(ConsomateurAgent consomateurAgent) {
        this.consomateurAgent = consomateurAgent;
    }

    public static void main(String[] args) {
        launch(ConsomateurContainer.class);
    }

    public void startContainer(){
        try{
            Runtime simpleContainer = Runtime.instance();
            Profile profile = new ProfileImpl(false);
            profile.setParameter(Profile.MAIN_HOST, "localhost");

            AgentContainer agentContainer = simpleContainer.createAgentContainer(profile);

            AgentController agentController = agentContainer.createNewAgent("consomatteur" , ConsomateurAgent.class.getName(),new Object[]{this});

            agentController.start();

        } catch (ControllerException e) {
            System.out.println(e);
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        startContainer();
        primaryStage.setTitle("consomatteur");
        BorderPane borderPane = new BorderPane();

        livres = FXCollections.observableArrayList();


        HBox hBox = new HBox();
        hBox.setSpacing(10);
        Label labelLivre = new Label("Livre :");


        TextField textFieldLivre = new TextField();
        Button buttonBuyLivre = new Button("Buy");

        hBox.getChildren().add(labelLivre);
        hBox.getChildren().add(textFieldLivre);
        hBox.getChildren().add(buttonBuyLivre);

        VBox vBox = new VBox();
        GridPane gridPane = new GridPane();
        ListView<String> listView = new ListView<String>(livres);
        gridPane.add(listView , 0,0);
        vBox.getChildren().add(gridPane);

        borderPane.setCenter(vBox);


        borderPane.setTop(hBox);

        Scene scene = new Scene(borderPane , 400 , 400);
        primaryStage.setScene(scene);
        primaryStage.show();

        addLivre(buttonBuyLivre , textFieldLivre,livres);
    }

    private void addLivre(Button button , TextField livreText, ObservableList<String> livres) {
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String mssg = livreText.getText();
                livres.add(mssg);
                GuiEvent guiEvent = new GuiEvent(this , 1);
                guiEvent.addParameter(mssg);
                consomateurAgent.onGuiEvent(guiEvent);
            }
        });
    }

    public void showMessage(GuiEvent guiEvent) {

        System.out.println("msg from rma !!");
        if ( guiEvent.getType() == 1 ) {
            String msg = guiEvent.getParameter(0).toString() ;
            livres.add(msg);
        }
    }
}
